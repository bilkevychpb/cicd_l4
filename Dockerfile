# =======================================================
# Stage 1 - Build/compile app using container
# =======================================================

# Build image has SDK and tools (Linux)
FROM python:3.6-alpine AS base

# Set timezone
RUN ln -snf /usr/share/zoneinfo/Europe/Kiev /etc/localtime && echo Europe/Kiev > /etc/timezone
RUN apk update && \
apk add --no-cache --virtual build-deps gcc python3-dev musl-dev && \
apk add postgresql-dev

# Add and set workdir

ADD . /app
WORKDIR /app

# Install requirements
RUN set -ex && \
    pip install --upgrade pip && \ 
    pip install python-dotenv && \
    pip install -r requirements.txt


# =======================================================
# Stage 2 - Copy add user set port
# =======================================================

FROM python:3.6-alpine

WORKDIR /app
COPY --from=base /app .


RUN adduser -D app \
    && chown -R app:app /app
USER app

ENV FLASK_APP=src/app.py

EXPOSE 8000:8000

#ENTRYPOINT [ "python" ]

CMD python setup.py && flask run --host=0.0.0.0 --port 8000